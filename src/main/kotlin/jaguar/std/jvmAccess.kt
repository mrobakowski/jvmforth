@file:Suppress("unused")

package jaguar.std

import jaguar.core.JaguarWord
import jaguar.core.InjectCompiler
import jaguar.core.InjectWord
import jaguar.core.Macro
import jaguar.core.compiler.Compiler
import jaguar.core.compiler.TypeError
import jaguar.core.word.ExternalWord
import jaguar.core.word.FieldAccessWord
import javassist.NotFoundException
import uy.klutter.core.jdk.nullIfBlank
import java.lang.reflect.Executable
import java.lang.reflect.Modifier

@Macro
@JaguarWord("""([^#]+)?#[^#]+""", isPattern = true, priority = 200)
fun getJvmMethod(@InjectWord classHashMethod: String, @InjectCompiler c: Compiler) {
    var (className, methodSignature) = classHashMethod.split('#', limit = 2).let { it[0] as String? to it[1] }
    className = className?.trim().nullIfBlank()
    methodSignature = methodSignature.trim()
    val clazz = className?.let { c.lookupClass(it) } ?: c.currentFunctionBuilder.stackEffect.outputs.lastOrNull() ?:
            throw TypeError("Could not infer the receiver type for method ${classHashMethod.trim()}")

    val method = try {
        getMethod(clazz, methodSignature, c.currentFunctionBuilder.stackEffect.outputs)
    } catch (e: AmbiguousCallException) {
        throw AmbiguousCallException(classHashMethod.trim())
    } ?: throw NotFoundException("could not find the method ${clazz.simpleName}#$methodSignature")

    ExternalWord(method).executeInCompileTime(c)
}

private fun getMethod(clazz: Class<*>?, methodSignature: String, contextTypes: List<Class<*>>): Executable? {
    if (clazz == null) return null

    val executables = if (methodSignature == "new" ||
            methodSignature.startsWith("new(") ||
            methodSignature.startsWith("new/")
    ) {
        clazz.constructors
    } else {
        clazz.declaredMethods
    }

    val (signature, arityS) = methodSignature.split("/").let { if (it.size == 2) it else listOf(it[0], "") }
    val arity = try {
        arityS.toInt()
    } catch (e: NumberFormatException) {
        -1
    }
    val (methodName, paramsString) = signature.split('(', limit = 2)
            .let { if (it.size == 2) it else listOf(it[0], "") }
    val params = paramsString.split(',', ')', '(').filter { !it.isNullOrBlank() }

    val method = executables.filter { matches(it, methodName, params, arity, contextTypes) }
            .let {
                if (it.size > 1) {
                    disambiguate(it, contextTypes)
                } else {
                    it
                }
            }
            .firstOrNull() ?: getMethod(clazz.superclass, methodSignature, contextTypes)

    return method
}

fun disambiguate(methods: List<Executable>, contextTypes: List<Class<*>>): List<Executable> {
    val methodsAndParams = methods.map {
        val params = it.parameterTypes.toMutableList()
        if (!Modifier.isStatic(it.modifiers)) params += it.declaringClass
        it to params
    }

    val maxParams = methodsAndParams.asSequence().map { it.second.size }.max()!!

    for (i in 0..maxParams-1) {
        for ((m, params) in methodsAndParams) {
            // TODO: do this according to which type is more specific
            params.getOrNull(i)?.let { type ->
                contextTypes.getOrNull(contextTypes.size - params.size + i)?.let {
                    if (type == it) return listOf(m)
                }
            }
        }
    }

    throw AmbiguousCallException("this will get patched in further up the call stack")
}

class AmbiguousCallException(call: String) : Exception("Ambiguous call $call")

private fun matches(
        method: Executable,
        methodName: String,
        params: List<String>,
        arity: Int,
        contextTypes: List<Class<*>>
): Boolean {
    if (methodName != "new" && method.name != methodName) return false
    if (arity != -1 && params.isNotEmpty() && arity != params.size)
        throw Exception("Parameter string length and arity mismatch")
    if ((params.isNotEmpty() && method.parameterCount != params.size) ||
            (arity != -1 && method.parameterCount != arity)) return false

    params.forEachIndexed { i, param ->
        if (param != method.parameterTypes[i].simpleName && param != method.parameterTypes[i].canonicalName)
            return false
    }

    if (params.isEmpty() && arity != -1) { // check types from context
        val methodParams = method.parameterTypes.toMutableList()
        if (!Modifier.isStatic(method.modifiers)) methodParams += method.declaringClass
        val start = contextTypes.size - methodParams.size

        methodParams.forEachIndexed { i, paramType ->
            contextTypes.getOrNull(start + i)?.let { if (!paramType.kotlin.javaObjectType.isAssignableFrom(it)) return false }
        }
    }

    return true
}

@Macro
@JaguarWord(""".*\.[^.]+""", isPattern = true, priority = 300)
fun getJvmField(@InjectWord word: String, @InjectCompiler c: Compiler) {
    val fullyQualifiedFieldName = word.trim()
    val (className, fieldName) = Regex("""(.*)\.([^.]+)""").matchEntire(fullyQualifiedFieldName)?.destructured ?:
            throw IllegalArgumentException("Word $fullyQualifiedFieldName is invalid field name")
    val clazz = c.lookupClass(className) ?: throw IllegalArgumentException("Cannot find class $className")
    val field = clazz.getDeclaredField(fieldName)

    FieldAccessWord(field).executeInCompileTime(c)
}
