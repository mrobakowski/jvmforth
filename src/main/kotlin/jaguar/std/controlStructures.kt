@file:Suppress("unused")

package jaguar.std

import jaguar.core.JaguarWord
import jaguar.core.InjectCompiler
import jaguar.core.Macro
import jaguar.core.compiler.*
import java.util.*

private data class IfEffectTriple(
        var contextEffect: StackEffect,
        var ifBodyEffect: StackEffect? = null,
        var elseBodyEffect: StackEffect? = null
)

private val ifEffectStack = arrayListOf<IfEffectTriple>()

private fun unifyIf(ifTypes: IfEffectTriple): StackEffect {
    val ifBody = ifTypes.ifBodyEffect!!
    val elseBody = ifTypes.elseBodyEffect ?: StackEffect.empty()

    (ifBody.inputs.size == elseBody.inputs.size && ifBody.outputs.size == elseBody.outputs.size) ||
            throw TypeError("if expression branches have effects of wrong arity")

    val inputs = ifBody.inputs.zip(elseBody.inputs).map { selectSubtype(it.first, it.second) }
    val outputs = ifBody.outputs.zip(elseBody.outputs).map { selectSupertype(it.first, it.second) }

    return ifTypes.contextEffect + StackEffect(inputs, outputs)
}

@Macro @JaguarWord("if") fun ifWord(@InjectCompiler c: Compiler) {
    val predicateName = "pred${UUID.randomUUID().toString().replace("-", "")}"
    c.currentFunctionBuilder.body +=
            DECL_VAR_STMT("boolean", predicateName,
                    CALL_METH_EXPR(CAST_EXPR("Boolean", JAG_STK_POP_EXPR), "booleanValue")
            ) + IF(predicateName)
    c.currentFunctionBuilder.stackEffect += StackEffect.consuming(Boolean::class.java)
    ifEffectStack += IfEffectTriple(c.currentFunctionBuilder.stackEffect)
}

@Macro @JaguarWord("else") fun elseWord(@InjectCompiler c: Compiler) {
    val current = ifEffectStack.last()
    val ifBodyEffect = c.currentFunctionBuilder.stackEffect.since(current.contextEffect)
    current.ifBodyEffect = ifBodyEffect
    c.currentFunctionBuilder.stackEffect = current.contextEffect
    c.currentFunctionBuilder.body += ELSE
}

@Macro @JaguarWord("then") fun thenWord(@InjectCompiler c: Compiler) {
    val current = ifEffectStack.last()
    val (contextEffect, ifBodyEffect, elseBodyEffect) = current

    if (ifBodyEffect != null) { // if with else case
        val elseEffect = c.currentFunctionBuilder.stackEffect.since(contextEffect)
        current.elseBodyEffect = elseEffect
        val resultStackEffect = unifyIf(current)
        c.currentFunctionBuilder.stackEffect = resultStackEffect
    } else { // if without else case
        val ifEffect = c.currentFunctionBuilder.stackEffect.since(contextEffect)
        current.ifBodyEffect = ifEffect
        val resultStackEffect = unifyIf(current)
        c.currentFunctionBuilder.stackEffect = resultStackEffect
    }

    ifEffectStack.removeAt(ifEffectStack.lastIndex)

    c.currentFunctionBuilder.body += END_IF
}

@Macro @JaguarWord("throw") fun throwWord(@InjectCompiler c: Compiler) {
    c.currentFunctionBuilder.body += THROW_STMT(JAG_STK_POP_EXPR)
    c.currentFunctionBuilder.stackEffect += StackEffect.consuming(Throwable::class.java)
}

/*
 * jaguar while loop - inspired by forth's indefinite loop
 *
 * jaguar```
 * begin xxx while yyy repeat
 * ```
 *
 * xxx: ( -- Boolean)
 * yyy: ( -- )
 *
 * java```
 * xxx
 * bool whileLoop123456 = Stack.pop();
 * while(whileLoop123456) {
 *     yyy
 *     xxx
 *     whileLoop123456 = Stack.pop();
 * }
 * ```
 */
private val whileLoopStack = mutableListOf<WhileLoopMeta>()

private data class WhileLoopMeta(var contextEffect: StackEffect, val uuid: UUID = UUID.randomUUID()) {
    val id = uuid.toString().replace("-", "")
    var typeBeforeLoopBody: StackEffect? = null
}

private fun whileLoopConditionStartMarker(wlm: WhileLoopMeta) = "\n//wlcs${wlm.id}\n"
private fun whileLoopConditionEndMarker(wlm: WhileLoopMeta) = "\n//wlce${wlm.id}\n"
private fun whileLoopConditionVar(wlm: WhileLoopMeta) = "wlcv${wlm.id}"
private fun whileLoopConditionVarDecl(wlm: WhileLoopMeta) =
        " boolean ${whileLoopConditionVar(wlm)};\n"

@Macro @JaguarWord("begin") fun beginWhileLoop(@InjectCompiler c: Compiler) {
    val wlm = WhileLoopMeta(c.currentFunctionBuilder.stackEffect)
    whileLoopStack += wlm
    c.currentFunctionBuilder.body += whileLoopConditionVarDecl(wlm)
    c.currentFunctionBuilder.body += whileLoopConditionStartMarker(wlm)
}

@Macro @JaguarWord("while") fun whileLoopEndCondition(@InjectCompiler c: Compiler) {
    val wlm = whileLoopStack.last()
    val se = c.currentFunctionBuilder.stackEffect.since(wlm.contextEffect)
    if (se != StackEffect.producing(Boolean::class.java))
        throw TypeError("While loop condition must be of the type ( -- Boolean), got $se")


    c.currentFunctionBuilder.body +=
            ASSIGN_STMT(whileLoopConditionVar(wlm),
                    CALL_METH_EXPR(CAST_EXPR("Boolean", JAG_STK_POP_EXPR), "booleanValue"))
    c.currentFunctionBuilder.body += whileLoopConditionEndMarker(wlm)
    c.currentFunctionBuilder.body += WHILE(whileLoopConditionVar(wlm))
    c.currentFunctionBuilder.stackEffect += StackEffect.consuming(Boolean::class.java)

    wlm.typeBeforeLoopBody = c.currentFunctionBuilder.stackEffect
}

@Macro @JaguarWord("repeat") fun endWhileLoop(@InjectCompiler c: Compiler) {
    val wlm = whileLoopStack.removeAt(whileLoopStack.lastIndex)
    val se = c.currentFunctionBuilder.stackEffect.since(wlm.typeBeforeLoopBody!!)
    if (se != StackEffect.empty())
        throw TypeError("While loop body must be of type ( -- ), got $se")

    val start = whileLoopConditionStartMarker(wlm)
    val end = whileLoopConditionEndMarker(wlm)

    val condition = c.currentFunctionBuilder.body.substringAfter(start, missingDelimiterValue = "").substringBefore(end)

    c.currentFunctionBuilder.body += replaceDeclarationAssignmentsWithAssignments(condition)
    c.currentFunctionBuilder.body += END_WHILE
}

fun replaceDeclarationAssignmentsWithAssignments(body: String): String {
    return body.replace(Regex("""/\*startDecl\*/(.*)/\*endDecl\*/""")) {
        it.groupValues[1].substringAfter("/*afterType*/")
    }
}
