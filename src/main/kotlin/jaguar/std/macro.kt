package jaguar.std

import jaguar.core.*
import jaguar.core.compiler.CALL_STAT_EXPR
import jaguar.core.compiler.Compiler
import jaguar.core.compiler.JAG_STK_PUSH_STMT
import jaguar.core.compiler.StackEffect
import jaguar.core.word.Word
import jaguar.core.word.asMatcher
import jaguar.util.escapeJavaLiteral
import org.intellij.lang.annotations.Language

@Macro @JaguarWord("macro:") fun startMacroDeclaration(@InjectCompiler c: Compiler) {
    startDeclaration(c)
    c.currentFunctionBuilder.isMacro = true
    c.dictionary += "compiler".asMatcher() to object : Word {
        override val name = "compiler".asMatcher()
        override val stackEffect = StackEffect(listOf(), listOf(Compiler::class.java))

        override fun executeInCompileTime(compiler: Compiler) {
            compiler.currentFunctionBuilder.body += JAG_STK_PUSH_STMT(
                    CALL_STAT_EXPR("jaguar.core.compiler.Compiler", "getCurrentCompiler"))
            compiler.currentFunctionBuilder.stackEffect += stackEffect
        }
    }
}

// region string literal regexps
@Language("regExp")
const val ESCAPE_REGEX: String = """`(\\.|[^`])*`"""
val escapeRegex = ESCAPE_REGEX.toRegex()
@Language("regExp")
const val ESCAPE_REGEX_MAYBE_WITHOUT_END: String = """`(\\.|[^`])*`?"""
@Language("regExp")
const val ESCAPE_REGEX_WITHOUT_BEGINNING: String = """(\\.|[^`])*`"""
// endregion

@Macro @JaguarWord(ESCAPE_REGEX_MAYBE_WITHOUT_END, isPattern = true)
fun prependCode(@InjectWord s: String, @InjectCompiler c: Compiler) {
    var code = s.run { if (!startsWith("`")) trimStart() else this }
    val pat = ESCAPE_REGEX_WITHOUT_BEGINNING.toPattern()

    while (!escapeRegex.matches(code)) {
        code += c.input.readUntilMatches(pat)
    }

    code = code.removeSurrounding("`")
    val fragments = extractEvalFragments(code)


    val finalCode = fragments.asReversed().joinToString(separator = "\n") { it.toPrepend() }
    c.input.prepend(finalCode)
}

sealed class Fragment {
    abstract fun toPrepend(): String

    class Literal(val code: String) : Fragment() {
        override fun toPrepend() = """
            ${'"' + code.escapeJavaLiteral() + '"'} compiler #getInput #prepend
        """
    }

    class Eval(val code: String) : Fragment() {
        override fun toPrepend() = """
            $code compiler #getInput #prepend
        """
    }
}

fun extractEvalFragments(code: String): List<Fragment> {
    val res = mutableListOf<Fragment>()
    var startIdx = 0

    while (startIdx < code.length) {
        val i = code.indexOf("${"$"}{", // damn you Kotlin string interpolation
                startIndex = startIdx)

        if (i == -1) { // no interpolation
            res += Fragment.Literal(code.substring(startIdx, code.length))
            break
        }

        var openBraces = 1
        var closedBraces = 0
        var j = i + 1
        while (openBraces != closedBraces && j < code.lastIndex) {
            j++
            when (code[j]) {
                '{' -> openBraces++
                '}' -> closedBraces++
            }
        }

        if (code[j] == '}') { // we have literal followed by interpolation
            if (startIdx != i)
                res += Fragment.Literal(code.substring(startIdx, i))
            res += Fragment.Eval(code.substring(i + 2, j))
            startIdx = j + 1
            continue
        } else { // degenerate ex. `${`
            res += Fragment.Literal(code.substring(startIdx, j + 1))
            startIdx = j + 2
        }
    }

    return res
}

@Macro @JaguarWord("java") fun runJava(@InjectCompiler c: Compiler) {
    val code = c.input.readUntil {
        val opens = it.count { it == '{' }
        val closeds = it.count { it == '}' }
        it.count() != 0 && opens == closeds && opens > 0
    }.trim()

    if (!code.startsWith('{')) throw RuntimeException("invalid java code block")
    c.currentFunctionBuilder.body += code
}

