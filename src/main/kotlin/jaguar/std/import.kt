package jaguar.std

import jaguar.core.JaguarWord
import jaguar.core.InjectCompiler
import jaguar.core.Macro
import jaguar.core.compiler.CALL_STAT_EXPR
import jaguar.core.compiler.Compiler
import jaguar.core.compiler.JAG_STK_PUSH_STMT
import jaguar.core.compiler.StackEffect
import jaguar.core.compiler.scope.ContextPath
import jaguar.core.compiler.scope.isFileScope
import jaguar.core.compiler.scope.popReplOrFunctionScope
import jaguar.util.escapeJavaLiteral
import jaguar.util.unescapeJavaLiteral
import java.nio.file.Paths

@Macro @JaguarWord("imports") fun pushImportList(@InjectCompiler c: Compiler) {
    val imports = c.imports.asSequence().map {
        """ "$it" """
    }.joinToString(separator = ",")
    c.currentFunctionBuilder.body += JAG_STK_PUSH_STMT(
            CALL_STAT_EXPR("java.util.Arrays", "asList", "new String[] {$imports}"))
    c.currentFunctionBuilder.stackEffect += StackEffect.producing(List::class.java)
}

@Macro @JaguarWord("include") fun includeFile(@InjectCompiler c: Compiler) {
    val pathString = getTrailingString(c)
    val path = Paths.get(c.topScope.get<ContextPath>().toString(), pathString).normalize()
    c.input.prepend(" popFileScope ")
    c.input.prepend(path.toFile().readText())
    c.input.prepend(""" pushFileScope "${path.toString().escapeJavaLiteral()}" """)

}

fun getTrailingString(c: Compiler): String {
    c.input.discardLeadingWhitespaces()
    val rawString = c.input.readUntilMatches(STRING_REGEX.toPattern())
    val string = rawString.run { removePrefix("\"").removeSuffix("\"") }.unescapeJavaLiteral()
    return string
}

@Macro @JaguarWord("pushFileScope") fun pushFileScope(@InjectCompiler c: Compiler) {
    c.throwIfUnterminatedFun()
    c.scopeStack.popReplOrFunctionScope()
    val filePath = getTrailingString(c)
    c.scopeStack.pushFileScope(Paths.get(filePath))
    c.scopeStack.pushReplScope()
}

@Macro @JaguarWord("popFileScope") fun popFileScope(@InjectCompiler c: Compiler) {
    while (!c.scopeStack.pop().isFileScope()) {} // pops all the scopes above nearest file scope and including it
    c.scopeStack.pushReplScope()
}