@file:Suppress("unused", "PLATFORM_CLASS_MAPPED_TO_KOTLIN")

package jaguar.std

import jaguar.core.JaguarWord
import jaguar.core.StackEffectAnnotation

@JaguarWord("+", stackEffect = StackEffectAnnotation(arrayOf(Integer::class, Integer::class), arrayOf(Integer::class)))
fun intAdd(a: Int, b: Int) = a + b
@JaguarWord("-", stackEffect = StackEffectAnnotation(arrayOf(Integer::class, Integer::class), arrayOf(Integer::class)))
fun intSub(a: Int, b: Int) = a - b
@JaguarWord("*", stackEffect = StackEffectAnnotation(arrayOf(Integer::class, Integer::class), arrayOf(Integer::class)))
fun intMul(a: Int, b: Int) = a * b
@JaguarWord("/", stackEffect = StackEffectAnnotation(arrayOf(Integer::class, Integer::class), arrayOf(Integer::class)))
fun intDiv(a: Int, b: Int) = a / b
@JaguarWord(">", stackEffect = StackEffectAnnotation(arrayOf(Integer::class, Integer::class), arrayOf(Boolean::class)))
fun intGreaterThan(a: Int, b: Int) = a > b
@JaguarWord("<", stackEffect = StackEffectAnnotation(arrayOf(Integer::class, Integer::class), arrayOf(Boolean::class)))
fun intLessThan(a: Int, b: Int) = a < b
@JaguarWord(">=", stackEffect = StackEffectAnnotation(arrayOf(Integer::class, Integer::class), arrayOf(Boolean::class)))
fun intGreaterThanEquals(a: Int, b: Int) = a >= b
@JaguarWord("<=", stackEffect = StackEffectAnnotation(arrayOf(Integer::class, Integer::class), arrayOf(Boolean::class)))
fun intLessThanEquals(a: Int, b: Int) = a <= b

@JaguarWord("d+", stackEffect = StackEffectAnnotation(arrayOf(Double::class, Double::class), arrayOf(Double::class)))
fun doubleAdd(a: Double, b: Double) = a + b
@JaguarWord("d-", stackEffect = StackEffectAnnotation(arrayOf(Double::class, Double::class), arrayOf(Double::class)))
fun doubleSub(a: Double, b: Double) = a - b
@JaguarWord("d*", stackEffect = StackEffectAnnotation(arrayOf(Double::class, Double::class), arrayOf(Double::class)))
fun doubleMul(a: Double, b: Double) = a * b
@JaguarWord("d/", stackEffect = StackEffectAnnotation(arrayOf(Double::class, Double::class), arrayOf(Double::class)))
fun doubleDiv(a: Double, b: Double) = a / b
@JaguarWord("d>", stackEffect = StackEffectAnnotation(arrayOf(Double::class, Double::class), arrayOf(Boolean::class)))
fun doubleGreaterThan(a: Double, b: Double) = a > b
@JaguarWord("d<", stackEffect = StackEffectAnnotation(arrayOf(Double::class, Double::class), arrayOf(Boolean::class)))
fun doubleLessThan(a: Double, b: Double) = a < b
@JaguarWord("d>=", stackEffect = StackEffectAnnotation(arrayOf(Double::class, Double::class), arrayOf(Boolean::class)))
fun doubleGreaterThanEquals(a: Double, b: Double) = a >= b
@JaguarWord("d<=", stackEffect = StackEffectAnnotation(arrayOf(Double::class, Double::class), arrayOf(Boolean::class)))
fun doubleLessThanEquals(a: Double, b: Double) = a <= b

@JaguarWord("s+", stackEffect = StackEffectAnnotation(arrayOf(String::class, String::class), arrayOf(String::class)))
fun stringAdd(a: String, b: String) = a + b