@file:Suppress("unused")

package jaguar.std

import jaguar.core.*
import jaguar.core.compiler.*
import jaguar.core.compiler.scope.isReplScope
import jaguar.core.compiler.scope.popReplOrFunctionScope
import jaguar.core.word.Word
import jaguar.core.word.WordMatcher
import jaguar.core.word.asMatcher
import jaguar.util.printStackTrace

@Macro @JaguarWord(";") fun finishDeclaration(@InjectCompiler c: Compiler) {
    val compiled = c.compileAndStartNext()

    val scope = c.scopeStack.popReplOrFunctionScope()

    if (compiled.isReplInvocation) {
        try {
            compiled()
        } catch (e: Exception) {
            e.printStackTrace()
        } finally {
            if (scope?.isReplScope() ?: false && scope?.parent?.isGlobalScope ?: false) {
                print("|")
                println(Stack.toString())
                c.mainPrompt()
            }
        }
    } else {
        c.dictionary += compiled.name to compiled
        if (scope?.parent?.isGlobalScope ?: false) {
            println("${compiled.name} defined\n${compiled.stackEffect}")
            c.mainPrompt()
        }
    }

    c.scopeStack.pushReplScope()
}

@Macro @JaguarWord(":") fun startDeclaration(@InjectCompiler c: Compiler) {
    c.throwIfUnterminatedFun()
    val name = c.input.readNextWord().trim()
    if (c.topScope.isReplScope()) c.scopeStack.pop()
    c.scopeStack.pushFunctionScope()
    c.startMethod(name)
}

@Macro @JaguarWord("(") fun declareType(@InjectWord w: String, @InjectCompiler c: Compiler) {
    val typeDecl = w.trimStart() + c.input.readUntil { it.endsWith(')') }
    val (inp, out) = typeDecl.removePrefix("(").removeSuffix(")").trim().split("--")
    val spaces = Regex("""\s+""")
    val inTypes = inp.trim().split(spaces).asSequence().filter { it.isNotEmpty() }.map { c.lookupClass(it)!! }.toList()
    val outTypes = out.trim().split(spaces).asSequence().filter { it.isNotEmpty() }.map { c.lookupClass(it)!! }.toList()

    val se = StackEffect(inTypes, outTypes)
    c.currentFunctionBuilder.expectedStackEffect = se

    val name = c.currentFunctionBuilder.nameMatcher
    c.dictionary += name to object : Word {
        override val name = name
        override val stackEffect = se

        override fun executeInCompileTime(compiler: Compiler) {
            c.currentFunctionBuilder.body += STMT(CALL_REC)
            c.currentFunctionBuilder.stackEffect += stackEffect
        }
    }
}

class LocalVariableAccessWord(val variableName: String, type: Class<*>) : Word {
    override val name: WordMatcher = variableName.asMatcher()
    override val stackEffect = StackEffect(listOf(), listOf(type))
    override fun executeInCompileTime(compiler: Compiler) {
        compiler.currentFunctionBuilder.body += JAG_STK_PUSH_STMT(variableName)
        compiler.currentFunctionBuilder.stackEffect += stackEffect
    }
}

@Macro @JaguarWord("->") fun defLocalVariable(@InjectCompiler c: Compiler) {
    val namesAndTypes = getCommaSpaceSeparatedWordsMaybeWithTypeAnnotations(c)

    c.currentFunctionBuilder.body += namesAndTypes.asReversed().map {
        val (name, type) = it
        DECL_VAR_STMT(type?.canonicalName ?: "Object", name, JAG_STK_POP_EXPR)
    }.joinToString(separator = "\n")

    val types = mutableListOf<Class<*>>()
    namesAndTypes.asReversed().forEachIndexed { i, nameAndType ->
        val type = nameAndType.second
        val outputs = c.currentFunctionBuilder.stackEffect.outputs
        val consumedType: Class<*> = type ?: outputs.lastOrNull() ?: Any::class.java
        types += consumedType
        c.currentFunctionBuilder.stackEffect += StackEffect.consuming(consumedType)
    }
    types.reverse()

    namesAndTypes.forEachIndexed { i, nameAndType ->
        val name = nameAndType.first
        c.dictionary += name.asMatcher() to LocalVariableAccessWord(name, types[i])
    }
}

@Macro @JaguarWord("=>") fun assign(@InjectCompiler c: Compiler) {
    val names = getCommaSpaceSeparatedWords(c)

    c.currentFunctionBuilder.body += names.asReversed().map {
        val varWord = c.dictionary.getWord(it) as LocalVariableAccessWord
        val type = varWord.stackEffect.outputs[0]
        c.currentFunctionBuilder.stackEffect += StackEffect.consuming(type)
        ASSIGN_STMT(it, JAG_STK_POP_EXPR)
    }.joinToString(separator = "\n")
}

private fun getCommaSpaceSeparatedWords(c: Compiler): List<String> {
    return mutableListOf<String>().apply {
        var name: String
        do {
            name = c.input.readNextWord().trim()
            add(name.removeSuffix(","))
        } while (name.endsWith(','))
    }
}

fun getCommaSpaceSeparatedWordsMaybeWithTypeAnnotations(c: Compiler): List<Pair<String, Class<*>?>> {
    return mutableListOf<Pair<String, Class<*>?>>().apply {
        var name: String
        var type: Class<*>?
        do {
            var endedWithComma = false
            name = c.input.readNextWord().trim()
            if (name.endsWith(":")) {
                val typeName = c.input.readNextWord().trim()
                if (typeName.endsWith(",")) endedWithComma = true
                type = c.lookupClass(typeName.removeSuffix(","))
            } else {
                type = null
            }

            if (name.endsWith(",")) endedWithComma = true

            add(name.removeSuffix(",").removeSuffix(":") to type)
        } while (endedWithComma)
    }
}

@Macro @JaguarWord("//.*", isPattern = true) fun comment(@InjectCompiler c: Compiler) {
    c.input.readUntil { it.endsWith("\n") }
}

@Macro @JaguarWord("as") fun cast(@InjectCompiler c: Compiler) {
    val clazz = c.lookupClass(c.input.readNextWord().trim())!!
    c.currentFunctionBuilder.stackEffect += StackEffect.consuming(Any::class.java) + StackEffect.producing(clazz)
}

