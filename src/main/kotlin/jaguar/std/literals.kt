@file:Suppress("unused")

package jaguar.std

import jaguar.core.*
import jaguar.core.compiler.CALL_STAT_EXPR
import jaguar.core.compiler.Compiler
import jaguar.core.compiler.JAG_STK_PUSH_STMT
import jaguar.core.compiler.StackEffect
import org.intellij.lang.annotations.Language

// TODO: add stack effects to the words

@Macro @JaguarWord("true", stackEffect = StackEffectAnnotation(arrayOf(), arrayOf(Boolean::class)))
fun trueWord(@InjectCompiler c: Compiler) {
    c.currentFunctionBuilder.body += " jaguar.core.Stack.push(Boolean.TRUE); "
    c.currentFunctionBuilder.stackEffect += StackEffect.producing(Boolean::class.java)
}

@Macro @JaguarWord("false", stackEffect = StackEffectAnnotation(arrayOf(), arrayOf(Boolean::class)))
fun falseWord(@InjectCompiler c: Compiler) {
    c.currentFunctionBuilder.body += JAG_STK_PUSH_STMT("Boolean.FALSE")
    c.currentFunctionBuilder.stackEffect += StackEffect.producing(Boolean::class.java)
}

@Macro @JaguarWord("null", stackEffect = StackEffectAnnotation(arrayOf(), arrayOf(Nothing::class)))
fun nullWord(@InjectCompiler c: Compiler) {
    c.currentFunctionBuilder.body += JAG_STK_PUSH_STMT("null")
    c.currentFunctionBuilder.stackEffect += StackEffect.producing(Nothing::class.java)
}

@Macro @JaguarWord("""-?\d+""", isPattern = true,
        stackEffect = StackEffectAnnotation(arrayOf(), arrayOf(Int::class)))
fun intLiteral(@InjectWord s: String, @InjectCompiler c: Compiler) {
    val i = s.trim().toInt()
    c.currentFunctionBuilder.body += JAG_STK_PUSH_STMT(CALL_STAT_EXPR("java.lang.Integer", "valueOf", "$i"))
    c.currentFunctionBuilder.stackEffect += StackEffect.producing(Int::class.java)
}

@Macro @JaguarWord("""-?\d+\.\d+""", isPattern = true,
        stackEffect = StackEffectAnnotation(arrayOf(), arrayOf(Double::class)))
fun doubleLiteral(@InjectWord s: String, @InjectCompiler c: Compiler) {
    val d = s.trim().toDouble()
    c.currentFunctionBuilder.body += JAG_STK_PUSH_STMT(CALL_STAT_EXPR("java.lang.Double", "valueOf", "$d"))
    c.currentFunctionBuilder.stackEffect += StackEffect.producing(Double::class.java)
}

// region string literal regexps
@Language("regExp")
const val STRING_REGEX: String = """"(\\.|[^\\"])*""""
val stringRegex = STRING_REGEX.toRegex()
@Language("regExp")
const val STRING_REGEX_MAYBE_WITHOUT_END: String = """"(\\.|[^\\"])*"?"""
@Language("regExp")
const val STRING_REGEX_WITHOUT_BEGINNING: String = """(\\.|[^\\"])*""""
// endregion

@Macro @JaguarWord(STRING_REGEX_MAYBE_WITHOUT_END, isPattern = true,
        stackEffect = StackEffectAnnotation(arrayOf(), arrayOf(String::class)))
fun stringLiteral(@InjectWord s: String, @InjectCompiler c: Compiler) {
    var string = s.run { if (!startsWith("\"")) trimStart() else this }
    val pat = STRING_REGEX_WITHOUT_BEGINNING.toPattern()

    while (!stringRegex.matches(string)) {
        string += c.input.readUntilMatches(pat)
    }

    string = string.replace("\n", "\\n")
    c.currentFunctionBuilder.body += JAG_STK_PUSH_STMT(string)
    c.currentFunctionBuilder.stackEffect += StackEffect.producing(String::class.java)
}

