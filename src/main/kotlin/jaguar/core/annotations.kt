package jaguar.core

import jaguar.core.compiler.StackEffect
import jaguar.core.word.WordMatcher
import org.intellij.lang.annotations.Language
import kotlin.reflect.KClass

@Target(AnnotationTarget.FUNCTION)
annotation class JaguarWord(@param:Language("regExp") val name: String,
                            val isPattern: Boolean = false,
                            val priority: Int = 100,
                            val stackEffect: StackEffectAnnotation = StackEffectAnnotation(arrayOf(), arrayOf()))

val JaguarWord.wordMatcher: WordMatcher
    get() = if (this.isPattern) {
        WordMatcher.Pattern(this.name, this.priority)
    } else {
        WordMatcher.Literal(this.name)
    }

@Target(AnnotationTarget.FUNCTION)
annotation class Macro

@Target(AnnotationTarget.VALUE_PARAMETER)
annotation class InjectWord

@Target(AnnotationTarget.VALUE_PARAMETER)
annotation class InjectCompiler

annotation class StackEffectAnnotation(val inputs: Array<KClass<*>>, val outputs: Array<KClass<*>>)

fun JaguarWord.stackEffect(): StackEffect {
    val inputs = stackEffect.inputs
    val outputs = stackEffect.outputs
    return StackEffect(inputs.map { it.javaObjectType }, outputs.map { it.javaObjectType })
}