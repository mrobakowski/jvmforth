package jaguar.core

import java.util.*
import kotlin.concurrent.getOrSet

object Stack {
    private val tl = ThreadLocal<MutableList<Any?>>()

    private val s: MutableList<Any?>
        get() = tl.getOrSet { ArrayList<Any?>(128) }

    @JvmStatic fun pop(): Any? = s.removeAt(s.size - 1)
    @JvmStatic fun push(x: Any?): Unit { s.add(x) }
    @JvmStatic fun clear() = s.clear()

    override fun toString() = s.joinToString(" ")
}