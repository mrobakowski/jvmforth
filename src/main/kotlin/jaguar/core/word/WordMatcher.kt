package jaguar.core.word

sealed class WordMatcher : (String) -> Boolean {
    abstract fun matches(s: String): Boolean
    override fun invoke(s: String) = matches(s)

    class Literal(val lit: String) : WordMatcher() {
        override fun matches(s: String) = s == lit
        override fun toString() = lit

        override fun equals(other: Any?): Boolean {
            if (this === other) return true
            if (other?.javaClass != javaClass) return false

            other as Literal

            if (lit != other.lit) return false

            return true
        }

        override fun hashCode(): Int {
            return lit.hashCode()
        }
    }

    class Pattern(val pat: Regex, val priority: Int = 100) : WordMatcher() {
        constructor(s: String, priority: Int = 100) : this(Regex(s), priority)
        override fun matches(s: String) = pat.matches(s)
        override fun toString() = "/$pat/"

        override fun equals(other: Any?): Boolean {
            if (this === other) return true
            if (other?.javaClass != javaClass) return false

            other as Pattern

            if (pat != other.pat) return false
            if (priority != other.priority) return false

            return true
        }

        override fun hashCode(): Int {
            var result = pat.hashCode()
            result = 31 * result + priority
            return result
        }
    }

    companion object {
        fun of(str: String) = Literal(str)
    }
}

fun String.asMatcher() = WordMatcher.of(this)