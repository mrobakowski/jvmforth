package jaguar.core.word

import jaguar.core.*
import jaguar.core.compiler.*
import java.lang.reflect.*
import java.lang.reflect.Method as JavaMethod

interface Word {
    /** e.g. "+" */
    val name: WordMatcher
    val stackEffect: StackEffect

    fun executeInCompileTime(compiler: Compiler)
}

// TODO: bring this back when Kotlin plugin works :/
//typealias CompileTimeAction = (Compiler) -> Unit

class CompiledWord constructor(fb: FunctionBuilder) : Word, () -> Unit {
    val isReplInvocation = fb.isReplInvocation

    override val name = fb.nameMatcher
    val fullyQualifiedName = fb.fullyQualifiedMethodName
    val functionRunnable = fb.runnable
    val isMacro = fb.isMacro
    override val stackEffect = fb.stackEffect

    override fun executeInCompileTime(compiler: Compiler) {
        if (isMacro) functionRunnable.run()
        else {
            compiler.currentFunctionBuilder.callVoid(fullyQualifiedName)
            compiler.currentFunctionBuilder.stackEffect += stackEffect
        }
    }

    override fun invoke() = functionRunnable.run()
}

class BuiltinWord(m: JavaMethod) : Word {
    init {
        assert(m.getAnnotation(JaguarWord::class.java) != null)
    }

    override val name = m.getAnnotation(JaguarWord::class.java).wordMatcher
    private val compileTimeAction = getCompileTimeActionForAnnotatedMethod(m)
    override val stackEffect = getStackEffectForAnnotatedMethod(m)

    override fun executeInCompileTime(compiler: Compiler) {
        compileTimeAction(compiler)
    }
}

class FieldAccessWord(f: Field) : Word {
    override val name = WordMatcher.of("${f.declaringClass.simpleName}.${f.name}")
    private val compilingAction = getCompilingActionForField(f)
    override val stackEffect = getStackEffectForField(f)

    override fun executeInCompileTime(compiler: Compiler) {
        compilingAction(compiler)
    }
}

class ExternalWord(m: Executable) : Word {
    override val name = WordMatcher.of("${m.declaringClass.simpleName}#${m.name}")
    private val compilingAction = getCompilingActionForExecutable(m)
    override val stackEffect = getStackEffectForExecutable(m)

    override fun executeInCompileTime(compiler: Compiler) {
        compilingAction(compiler)
    }
}

//region builtin method adding functions
private fun getStackEffectForAnnotatedMethod(m: JavaMethod): StackEffect {
    val se = m.getAnnotation(JaguarWord::class.java).stackEffect()
    val isMacro = m.isAnnotationPresent(Macro::class.java)
    if (isMacro) {
        return se
    }

    val seForExec = getStackEffectForExecutable(m)
    if (seForExec != se)
        System.err.println("Wrong stack effect annotation on $m")

    return seForExec
}


private fun getCompileTimeActionForAnnotatedMethod(method: JavaMethod): (Compiler) -> Unit {
    if (!Modifier.isStatic(method.modifiers)) {
        throw IllegalArgumentException("Only static functions supported for now: $method")
    }

    val isMacro = method.isAnnotationPresent(Macro::class.java)
    if (isMacro) {
        return getCompileTimeActionForMacro(method)
    } else {
        return getCompilingActionForExecutable(method)
    }
}

val Executable.returnType: Class<*> get() = when (this) {
    is JavaMethod -> this.returnType
    is Constructor<*> -> this.declaringClass
    else -> throw IllegalStateException("Executable is not Method nor Constructor")
}

private fun getStackEffectForExecutable(exec: Executable) =
        StackEffect(exec.parameters.map { it.type.kotlin.javaObjectType } +
                if (!(Modifier.isStatic(exec.modifiers) || exec is Constructor<*>)) listOf(exec.declaringClass) else listOf(),
                if (exec.returnType in arrayOf(Nothing::class.java, Unit::class.java, Void.TYPE))
                    listOf()
                else
                    listOf(exec.returnType.kotlin.javaObjectType)
        )

private fun getCompilingActionForExecutable(exec: Executable): (Compiler) -> Unit {
    if (exec.returnType !in arrayOf(Nothing::class.java, Unit::class.java, Void.TYPE)
            || exec.parameters.isNotEmpty() || !Modifier.isStatic(exec.modifiers)) {
        return wrapFunction(exec)
    } else {
        val stackEffect = getStackEffectForExecutable(exec)
        return when (exec) {
            is JavaMethod -> { compiler ->
                compiler.currentFunctionBuilder.body += STMT(CALL_STAT_EXPR(exec.declaringClass.canonicalName, exec.name))
                compiler.currentFunctionBuilder.stackEffect += stackEffect
            }
            is Constructor<*> -> throw IllegalStateException("Constructor without return type?")
            else -> throw IllegalStateException("Executable is not Method nor Constructor")
        }
    }
}

//region primitive to wrapper mapping
@Suppress("PLATFORM_CLASS_MAPPED_TO_KOTLIN")
private val primitives: Map<Class<*>, Pair<Class<*>, String>> = mapOf(
        java.lang.Byte.TYPE to (java.lang.Byte::class.java to "byteValue"),
        java.lang.Long.TYPE to (java.lang.Long::class.java to "longValue"),
        java.lang.Short.TYPE to (java.lang.Short::class.java to "shortValue"),
        java.lang.Float.TYPE to (java.lang.Float::class.java to "floatValue"),
        java.lang.Double.TYPE to (java.lang.Double::class.java to "doubleValue"),
        java.lang.Integer.TYPE to (java.lang.Integer::class.java to "intValue"),
        java.lang.Boolean.TYPE to (java.lang.Boolean::class.java to "booleanValue"),
        java.lang.Character.TYPE to (java.lang.Character::class.java to "charValue")
)
//endregion

private fun getStackEffectForField(f: Field) =
        StackEffect(if (Modifier.isStatic(f.modifiers)) listOf() else listOf(f.declaringClass), listOf(f.type))

private fun getCompilingActionForField(f: Field): (Compiler) -> Unit {
    val popOperand = if (Modifier.isStatic(f.modifiers)) {
        ""
    } else {
        val thisType = f.declaringClass.canonicalName
        DECL_VAR_STMT(thisType, "that", CAST_EXPR(thisType, JAG_STK_POP_EXPR))
    }

    val rawAccess = if (Modifier.isStatic(f.modifiers)) {
        ACCS_FLD(f.declaringClass.canonicalName, f.name)
    } else {
        ACCS_FLD("that", f.name)
    }

    val wrappedAccess = if (f.type in primitives) {
        val wrapperType = primitives[f.type]!!.first.canonicalName
        JAG_STK_PUSH_STMT(CALL_STAT_EXPR(wrapperType, "valueOf", rawAccess))
    } else {
        JAG_STK_PUSH_STMT(rawAccess)
    }

    val call = popOperand + wrappedAccess
    val stackEffect = getStackEffectForField(f)
    return { compiler ->
        compiler.currentFunctionBuilder.body += call
        compiler.currentFunctionBuilder.stackEffect += stackEffect
    }
}

private fun wrapFunction(method: Executable): (Compiler) -> Unit {
    var params = method.parameters.mapIndexed { i, parameter ->
        val type = parameter.type
        // TODO: decide if @InjectWord makes sense in non-macro invocations
        if (type in primitives) {
            val (wrapper, primitiveGetter) = primitives[type]!!
            DECL_VAR_STMT(type.canonicalName, "p$i",
                    CALL_METH_EXPR(CAST_EXPR(wrapper.canonicalName, JAG_STK_POP_EXPR), primitiveGetter)
            )
        } else {
            DECL_VAR_STMT(type.canonicalName, "p$i", CAST_EXPR(type.canonicalName, JAG_STK_POP_EXPR))
        }
    }.asReversed().joinToString(separator = "")

    // prepend "this" if not static
    if (!Modifier.isStatic(method.modifiers) && method !is Constructor<*>) {
        val className = method.declaringClass.canonicalName
        params = DECL_VAR_STMT(className, "thisParam", CAST_EXPR(className, JAG_STK_POP_EXPR)) + params
    }

    val args = (0..method.parameterCount - 1).map { "p$it" }

    val rawCall = if (method is Constructor<*>) {
        CALL_CONSTR_EXPR(method.declaringClass.canonicalName, *args.toTypedArray())
    } else if (Modifier.isStatic(method.modifiers)) {
        CALL_STAT_EXPR(method.declaringClass.canonicalName, method.name, *args.toTypedArray())
    } else {
        CALL_METH_EXPR("thisParam", method.name, *args.toTypedArray())
    }

    val call = if (method.returnType in arrayOf(Nothing::class.java, Unit::class.java, Void.TYPE)) {
        params + STMT(rawCall)
    } else {
        if (method.returnType in primitives)
            params + JAG_STK_PUSH_STMT(CAST_EXPR("${'$'}w", rawCall))
        else
            params + JAG_STK_PUSH_STMT(rawCall)
    }

    val stackEffect = getStackEffectForExecutable(method)
    return { compiler ->
        compiler.currentFunctionBuilder.body += call
        compiler.currentFunctionBuilder.stackEffect += stackEffect
    }
}

private fun getCompileTimeActionForMacro(method: JavaMethod): (Compiler) -> Unit {
    if (method.returnType !in arrayOf(Nothing::class.java, Unit::class.java, Void.TYPE)) {
        throw IllegalArgumentException("Only void-returning functions supported as macros: $method")
    }

    val args: Array<Any?> = Array(method.parameterCount, { null })
    method.parameters.forEachIndexed { i, parameter ->
        for (annotation in parameter.annotations) {
            when (annotation) {
                is InjectWord -> injectWord(parameter, i, args)
                is InjectCompiler -> injectCompiler(parameter, i, args)
            }
        }
    }

    return { compiler ->
        val argsClone = arrayOf(*args)
        for (i in argsClone.indices) {
            when (argsClone[i]) {
                is CompilerPlaceholder -> argsClone[i] = compiler
                is WordPlaceholder -> argsClone[i] = compiler.currentRawWord
            }
        }
        method.invoke(null, *argsClone)
    }
}

private object CompilerPlaceholder

private fun injectCompiler(parameter: Parameter, i: Int, args: Array<Any?>) {
    if (parameter.type.isAssignableFrom(Compiler::class.java)) {
        if (args[i] != null) {
            throw IllegalArgumentException("Parameter #$i already taken! Aborting!")
        }
        args[i] = CompilerPlaceholder
    } else {
        throw IllegalArgumentException("Could not inject compiler into parameter #$i! Aborting!")
    }
}

private object WordPlaceholder

private fun injectWord(parameter: Parameter, i: Int, args: Array<Any?>) {
    if (parameter.type.isAssignableFrom(String::class.java)) {
        if (args[i] != null) {
            throw IllegalArgumentException("Parameter #$i already taken! Aborting!")
        }
        args[i] = WordPlaceholder
    } else {
        throw IllegalArgumentException("Could not inject word into parameter #$i! Aborting!")
    }
}
//endregion