package jaguar.core.compiler

import jaguar.core.word.WordMatcher
import jaguar.core.word.CompiledWord
import jaguar.util.escapeJaguarName
import javassist.*
import javassist.bytecode.CodeIterator
import javassist.bytecode.ConstPool
import javassist.convert.Transformer

class FunctionBuilder(val compiler: Compiler, methodName: String? = null, var body: String = "") {
    var isReplInvocation: Boolean = true
        private set

    var name: String = "<placeholder>"
        set(value) {
            if (value.isNullOrBlank()) {
                field = "Repl${replNameCounter++}"
                packageName = "jaguar.generated"
                isReplInvocation = true
            } else {
                field = value
                isReplInvocation = false
            }
        }

    val nameMatcher: WordMatcher get() =
    if (name.startsWith("/") && name.endsWith("/"))
        WordMatcher.Pattern(name.removePrefix("/").removeSuffix("/"))
    else
        WordMatcher.of(name)

    private var clashCounter = 0
    val simpleClassName: String
        get() = if (isReplInvocation) name else "JaguarWord${name.escapeJaguarName()}$clashCounter"
    val simpleMethodName: String
        get() = "$simpleClassName#staticRun"

    var packageName: String = "<placeholder>"

    var stackEffect: StackEffect = StackEffect(listOf(), listOf())

    var expectedStackEffect: StackEffect? = null

    internal val runnable by lazy { build() }

    var isBuilt = false
        private set

    var fullyQualifiedMethodName: String
        get() = (if (packageName.isNullOrBlank()) "" else packageName + ".") + simpleMethodName
        set(value) {
            setFromCompoundName(value, init = false)
        }

    val fullyQualifiedClassName: String
        get() = (if (packageName.isNullOrBlank()) "" else packageName + ".") + simpleClassName

    init {
        setFromCompoundName(methodName)
    }

    private fun setFromCompoundName(compoundName: String?, init: Boolean = true) {
        val cn = compoundName ?: ""
        val match = Regex("""((.*)\.)?([^.]*)""").matchEntire(cn)
        val n = match?.groupValues?.get(3)
        if (n == null) {
            System.err.println("""Invalid identifier: "$cn"""")
            if (init) {
                name = ""
            }
        } else {
            name = n
            packageName = match?.groupValues?.get(2) ?: ""
        }
    }

    private fun compatible(expected: StackEffect, actual: StackEffect): Boolean {
        return expected.inputs.zip(actual.inputs).all { it.second.isAssignableFrom(it.first) } &&
                expected.outputs.zip(actual.outputs).all { it.first.isAssignableFrom(it.second) }
    }

    fun compile(): CompiledWord {
        if (expectedStackEffect != null && !compatible(expectedStackEffect!!, stackEffect)) {
            throw TypeError("expected type and actual type differ:\n\t\t" +
                    "Expected: $expectedStackEffect\n\t\tActual: $stackEffect")
        }

        runnable // prime the compilation
        return CompiledWord(this)
    }

    private fun build(): Runnable {
        val cp = ClassPool.getDefault()
        val cc = tryMakeClass(cp)

        cp.clearImportedPackages()
        compiler.imports.forEach {
            cp.importPackage(it)
        }

        val unoptimized = try {
            CtMethod.make("""public static void staticRun() { $body }""", cc)
        } catch (e: CannotCompileException) {
            throw e
        }
//        val staticRun = unoptimized
        val staticRun = optimize(unoptimized)
        cc.addMethod(staticRun)
        val run = CtMethod.make("""public void run() { this.staticRun(); }""", cc)
        cc.addMethod(run)
        cc.addInterface(cp.get("java.lang.Runnable"))

        cc.debugWriteFile("./generatedClasses")

        val compiledMethod = cc.toClass().newInstance() as Runnable

        isBuilt = true
        return compiledMethod
    }

    private fun optimize(unoptimized: CtMethod): CtMethod {
        removeRedundantStackPushPop(unoptimized)
        removeRedundantCasts(unoptimized)

        return unoptimized
    }

    private fun removeRedundantStackPushPop(unoptimized: CtMethod) {
        val transformer = object : Transformer(null) {
            override fun transform(
                    clazz: CtClass,
                    pos: Int,
                    iterator: CodeIterator,
                    cp: ConstPool
            ): Int {
                if (iterator.byteAt(pos) == INVOKESTATIC) {
                    var index = iterator.u16bitAt(pos + 1)
                    if (cp.getMethodrefName(index) == "push" &&
                            cp.getMethodrefClassName(index) == "jaguar.core.Stack") {
                        if (iterator.byteAt(pos + 3) == INVOKESTATIC) {
                            index = iterator.u16bitAt(pos + 4)
                            if (cp.getMethodrefName(index) == "pop" &&
                                    cp.getMethodrefClassName(index) == "jaguar.core.Stack") {
                                val NOP = NOP.toByte()
                                iterator.write(byteArrayOf(
                                        NOP,
                                        NOP,
                                        NOP,

                                        NOP,
                                        NOP,
                                        NOP
                                ), pos)
                            }
                        } else if (iterator.byteAt(pos + 3) == GOTO) {
                            val offset = iterator.s16bitAt(pos + 4)
                            val pos2 = pos + offset
                            if (clazz.simpleName == "Repl69") {
                                1 + 1
                            }
                            if (iterator.byteAt(pos2) == INVOKESTATIC) {
                                var index = iterator.u16bitAt(pos2 + 1)
                                if (cp.getMethodrefName(index) == "push" &&
                                        cp.getMethodrefClassName(index) == "jaguar.core.Stack") {
                                    if (iterator.byteAt(pos2 + 3) == INVOKESTATIC) {
                                        index = iterator.u16bitAt(pos2 + 4)
                                        if (cp.getMethodrefName(index) == "pop" &&
                                                cp.getMethodrefClassName(index) == "jaguar.core.Stack") {
                                            val NOP = NOP.toByte()
                                            iterator.write(byteArrayOf(NOP, NOP, NOP), pos)
                                            iterator.write(byteArrayOf(
                                                    NOP,
                                                    NOP,
                                                    NOP,

                                                    NOP,
                                                    NOP,
                                                    NOP
                                            ), pos2)
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                return pos
            }
        }

        unoptimized.instrument(object : CodeConverter() {
            init {
                transformers = transformer
            }
        })
    }

    private fun removeRedundantCasts(unoptimized: CtMethod) {
        val transformer = object : Transformer(null) {
            override fun transform(
                    clazz: CtClass,
                    pos: Int,
                    iterator: CodeIterator,
                    cp: ConstPool
            ): Int {
                val typeGetter: (ConstPool, Int) -> String = when (iterator.byteAt(pos)) {
                    GETSTATIC, GETFIELD -> { cp: ConstPool, i: Int -> cp.getFieldrefType(i) }
                    INVOKESPECIAL, INVOKESTATIC, INVOKEVIRTUAL -> { cp: ConstPool, i: Int ->
                        cp.getMethodrefType(i).split(")")[1]
                    }
                    INVOKEINTERFACE -> { cp: ConstPool, i: Int ->
                        cp.getInterfaceMethodrefType(i).split(")")[1]
                    }
                    else -> return pos
                }

                var i = 3
                while (iterator.byteAt(pos + i) == NOP) i++
                if (iterator.byteAt(pos + i) == CHECKCAST) {
                    val fieldIndex = iterator.s16bitAt(pos + 1)
                    val fieldType = typeGetter(cp, fieldIndex)
                    val castIndex = iterator.s16bitAt(pos + i + 1)
                    val castClass = cp.getClassInfo(castIndex).replace(".", "/").let {
                        if (it.startsWith("[")) it else "L$it;"
                    }
                    if (fieldType == castClass) {
                        val NOP = NOP.toByte()
                        iterator.write(byteArrayOf(NOP, NOP, NOP), pos + i)
                    }
                }

                return pos
            }
        }

        unoptimized.instrument(object : CodeConverter() {
            init {
                transformers = transformer
            }
        })
    }

    private fun tryMakeClass(cp: ClassPool): CtClass {
        while (true) try {
            return cp.makeClass(fullyQualifiedClassName)
        } catch (re: RuntimeException) {
            if (re.message?.contains("frozen class (cannot edit)") ?: false) {
                clashCounter++
            } else {
                throw re
            }
        }
    }

    companion object {
        var replNameCounter = 0
    }

    fun callVoid(fullyQualifiedName: String) {
        body += " $fullyQualifiedName();\n"
    }

    var isMacro = false
}

