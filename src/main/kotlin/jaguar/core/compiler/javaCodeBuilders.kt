package jaguar.core.compiler

private const val STACK: String = "jaguar.core.Stack"

const val JAG_STK_POP_EXPR: String = " $STACK.pop()"
fun JAG_STK_PUSH_STMT(expr: String) = " $STACK.push($expr);\n"
fun CAST_EXPR(t: String, expr: String) = " (($t)$expr)"
fun CALL_METH_EXPR(receiver: String, methodName: String, vararg args: String) =
        " $receiver.$methodName(${args.joinToString(", ")})"
fun CALL_STAT_EXPR(clazz: String, methodName: String, vararg args: String) =
        " $clazz#$methodName(${args.joinToString(", ")})"
fun CALL_CONSTR_EXPR(clazz: String, vararg args: String) =
        " new $clazz(${args.joinToString(",")})"
fun DECL_VAR_STMT(t: String, name: String, init: String) =
        " /*startDecl*/ $t /*afterType*/ $name = $init;/*endDecl*/\n"
fun ASSIGN_STMT(varName: String, expr: String) =
        " $varName = $expr;\n"
fun ACCS_FLD(receiver: String, fieldName: String) = "$receiver.$fieldName"
fun STMT(expr: String) = " $expr;\n"
fun THROW_STMT(expr: String) = " throw ((Throwable)$expr);\n"
fun IF(predicate: String) = " if ($predicate) {\n"
const val ELSE: String = " } else {\n"
const val END_IF: String = " }\n"
fun WHILE(predicate: String) = " while ($predicate) {\n"
const val END_WHILE: String = " }\n"
const val CALL_REC: String = "staticRun()"


