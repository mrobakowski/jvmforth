package jaguar.core.compiler

import jaguar.core.compiler.scope.ScopeAware
import uy.klutter.core.common.verifiedWith
import java.util.*

class TypeError(message: String = "TYPE ERROR") : Exception(message)

data class StackEffect(val inputs: List<Class<*>>, val outputs: List<Class<*>>, val changes: List<StackEffect> = emptyList()) {
    operator fun plus(that: StackEffect): StackEffect {
        val inp = ArrayDeque(this.inputs)
        val out = ArrayDeque(that.outputs)

        val err = TypeError("Incompatible stack effects: FIRST: $this, SECOND: $that")

        fun overflow(bigger: List<Class<*>>, smaller: List<Class<*>>, result: ArrayDeque<Class<*>>, swapAssignment: Boolean) {
            tailTypesCompatible(bigger, smaller, swapAssignment) || throw err
            val rest = bigger.dropLast(smaller.size)
            rest.asReversed().forEach { result.addFirst(it) }
        }

        if (this.outputs.size > that.inputs.size) {
            overflow(this.outputs, that.inputs, out, swapAssignment = true)
        } else if (this.outputs.size < that.inputs.size) {
            overflow(that.inputs, this.outputs, inp, swapAssignment = false)
        } else { //equal sizes
            typesCompatible(this.outputs, that.inputs, swapAssignment = true) || throw err
        }

        return StackEffect(inp.toList(), out.toList(), changes + that)
    }

    fun since(other: StackEffect): StackEffect {
        if (this === other) return empty()
        var i = changes.indexOfLast { it === other }
        if (i == -1)
            i = changes.asSequence().zip(other.changes.asSequence()).takeWhile { it.first == it.second }.count() - 1

        return changes.drop(i + 1).fold(empty()) { acc, x -> acc + x }
    }

    override fun toString() =
            "(${inputs.joinToString(separator = " ") {
                it.canonicalName
            }} -- ${outputs.joinToString(separator = " ") {
                it.canonicalName
            }})"

    companion object {
        fun empty() = StackEffect(listOf(), listOf())
        fun consuming(vararg types: Class<*>) =
                StackEffect(types.asSequence().map { it.kotlin.javaObjectType }.toList(), listOf())

        fun producing(vararg types: Class<*>) =
                StackEffect(listOf(), types.asSequence().map { it.kotlin.javaObjectType }.toList())
    }

    override fun equals(other: Any?): Boolean {
        return other is StackEffect && other.inputs == this.inputs && other.outputs == this.outputs
    }
}

data class TypeStack(private val l: MutableList<Class<*>>) : ScopeAware {
    fun apply(se: StackEffect) = l.verifiedWith {
        tailTypesCompatible(this, se.inputs, swapAssignment = true) ||
                throw TypeError("Current type stack doesn't end with ${se.inputs} (type stack: $l)")
    }.apply {
        for (i in 0..se.inputs.size) removeAt(lastIndex)
        addAll(se.outputs)
    }
}

private fun tailTypesCompatible(list: List<Class<*>>, end: List<Class<*>>, swapAssignment: Boolean) =
        typesCompatible(list.takeLast(end.size), end, swapAssignment)

private fun typesCompatible(a: List<Class<*>>, b: List<Class<*>>, swapAssignment: Boolean) =
        a.asSequence().zip(b.asSequence()).all {
            val (aa, bb) = it
            if (swapAssignment)
                bb.isAssignableFrom(aa) || aa == Nothing::class.java
            else
                aa.isAssignableFrom(bb) || bb == Nothing::class.java
        }

fun selectSupertype(a: Class<*>, b: Class<*>): Class<*> {
    if (a.isAssignableFrom(b) || b === Nothing::class.java) return a
    if (b.isAssignableFrom(a) || a === Nothing::class.java) return b

    return Any::class.java
}

fun selectSubtype(a: Class<*>, b: Class<*>): Class<*> {
    if (a.isAssignableFrom(b)) return b
    if (b.isAssignableFrom(a)) return a

    return Nothing::class.java
}