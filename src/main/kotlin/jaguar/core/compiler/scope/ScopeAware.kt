package jaguar.core.compiler.scope

interface ScopeAware {
    fun setScope(s: AbstractScope) {}
}