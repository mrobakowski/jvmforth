package jaguar.core.compiler.scope

import jaguar.core.JaguarWord
import jaguar.core.word.WordMatcher
import jaguar.core.word.asMatcher
import jaguar.core.word.BuiltinWord
import jaguar.core.word.Word
import org.reflections.Reflections
import org.reflections.scanners.MethodAnnotationsScanner
import org.reflections.util.ClasspathHelper
import java.util.*
import kotlin.properties.Delegates
import java.lang.reflect.Method as JavaMethod


open class Dictionary : ScopeAware {
    var _scope by Delegates.notNull<AbstractScope>()
    override fun setScope(s: AbstractScope) {
        _scope = s
    }

    val dict: MutableMap<WordMatcher, Word> = HashMap()

    fun getWord(key: String): Word? {
        val res = getByLiteral(key) ?: getByPattern(key)
        if (res != null) return res

        return _scope.getClosestAncestor<Dictionary>()?.getWord(key)
    }

    fun getByLiteral(literal: String): Word? {
        if (literal.asMatcher() in dict) return dict[literal.asMatcher()]
        return null
    }

    fun getByPattern(key: String): Word? {
        val patternWords = dict.keys.filterIsInstance<WordMatcher.Pattern>().sortedBy { it.priority }

        return patternWords
                .firstOrNull { it.matches(key) }
                ?.let { dict[it] }
    }

    fun addWord(matcher: WordMatcher, func: Word) {
        dict += matcher to func
    }

    operator fun plusAssign(pair: Pair<WordMatcher, Word>) = addWord(pair.first, pair.second)

    fun addBuiltinWords() {
        val reflections = Reflections("jaguar", ClasspathHelper.forClassLoader(), MethodAnnotationsScanner())
        val forthWords = reflections.getMethodsAnnotatedWith(JaguarWord::class.java)

        dict.putAll(forthWords.map { BuiltinWord(it).run { name to this } })

        println("Finished adding builtin words")
    }

    override fun toString(): String {
        return "Dictionary[${dict.size}]"
    }
}