package jaguar.core.compiler.scope

object FunctionScopeMarker : ScopeAware

fun AbstractScope.isFunctionScope() = has<FunctionScopeMarker>()

object ReplScopeMarker : ScopeAware

fun AbstractScope.isReplScope() = has<ReplScopeMarker>()

object FileScopeMarker : ScopeAware

fun AbstractScope.isFileScope() = has<FileScopeMarker>()

fun ScopeStack.popReplOrFunctionScope() =
        if (top.isFunctionScope() || top.isReplScope())
            pop()
        else null
