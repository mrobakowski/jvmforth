package jaguar.core.compiler.scope

data class FileName(val name: String): ScopeAware {
    override fun toString(): String {
        return name
    }
}