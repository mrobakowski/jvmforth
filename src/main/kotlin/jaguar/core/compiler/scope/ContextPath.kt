package jaguar.core.compiler.scope

import java.nio.file.Path

data class ContextPath(val path: Path): ScopeAware {
    override fun toString(): String {
        return path.toString()
    }
}