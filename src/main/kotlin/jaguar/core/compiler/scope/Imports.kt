package jaguar.core.compiler.scope

import org.intellij.lang.annotations.Language
import org.reflections.Reflections
import org.reflections.scanners.ResourcesScanner
import org.reflections.scanners.SubTypesScanner
import org.reflections.util.ClasspathHelper
import org.reflections.util.ConfigurationBuilder
import org.reflections.util.FilterBuilder
import java.lang.reflect.Modifier
import kotlin.properties.Delegates

class Imports: ScopeAware, Map<String, String> {
    private var _scope by Delegates.notNull<AbstractScope>()

    override fun setScope(s: AbstractScope) {
        _scope = s
    }

    private val _imports: MutableMap<String, String> = mutableMapOf()

    val imports: Map<String, String> get() = (parent?.imports ?: mapOf()) + _imports

    private val parent: Imports?
        get() = _scope.getClosestAncestor<Imports>()

    fun import(importName: String) {
        if (importName.endsWith(".*")) deglobAndImport(importName)
        else importNonGlob(importName)
    }

    private fun deglobAndImport(importName: String) {
        assert(importName.endsWith(".*"))
        val packageName = importName.removeSuffix(".*")
        @Language("RegExp")
        val classSuffixRegex = """\.[^.]+\.class"""

        val reflections = Reflections(ConfigurationBuilder()
                .setScanners(SubTypesScanner(false), ResourcesScanner())
                .setUrls(ClasspathHelper.forClassLoader(
                        ClasspathHelper.contextClassLoader(),
                        ClasspathHelper.staticClassLoader()))
                .filterInputsBy(FilterBuilder().include(packageName.replace(".", "\\.") + classSuffixRegex)))

        val classes = reflections.getSubTypesOf(Any::class.java)

        classes.asSequence()
                .map { it.name }
                .filterNotNull()
                .forEach { importNonGlob(it) }
    }

    private fun importNonGlob(importName: String) {
        val clazz = Class.forName(importName)
        if (Modifier.isPrivate(clazz.modifiers)) return
        _imports += (clazz.canonicalName ?: clazz.name).removePrefix(clazz.`package`.name + ".") to clazz.name
    }

    // region Map implementation
    override val entries: Set<Map.Entry<String, String>>
        get() = imports.entries
    override val keys: Set<String>
        get() = imports.keys
    override val size: Int
        get() = imports.size
    override val values: Collection<String>
        get() = imports.values

    override fun containsKey(key: String) = imports.containsKey(key)

    override fun containsValue(value: String) = imports.containsValue(value)

    override fun get(key: String) = imports[key]

    override fun isEmpty() = imports.isEmpty()
    // endregion

    override fun toString(): String {
        return "Imports[${_imports.size}]"
    }
}

fun imports(block: ImportsHelper.() -> Unit): Imports {
    val i = Imports()
    ImportsHelper(i).apply(block)
    return i
}

class ImportsHelper(val imports: Imports) {
    operator fun String.unaryPlus() {
        imports.import(this)
    }
}