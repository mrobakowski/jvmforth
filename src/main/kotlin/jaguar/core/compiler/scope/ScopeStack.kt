package jaguar.core.compiler.scope

import java.nio.file.Path

class ScopeStack(val scopes: MutableList<AbstractScope> = mutableListOf(GlobalScope)) {
    val top: AbstractScope get() = scopes.last()

    fun push(scope: AbstractScope) {
        scopes += scope
    }

    fun pop(): AbstractScope {
        return maybePop() ?: throw IllegalStateException("Cannot pop the global scope")
    }

    fun maybePop(): AbstractScope? {
        val s = top
        if (s is GlobalScope) return null
        scopes.removeAt(scopes.lastIndex)
        return s
    }

    /**
     * Pushes new scope without new Dictionary
     */
    fun pushFileScope(filePath: Path) {
        push(Scope(top).apply {
            remove<Dictionary>()
            add(FileScopeMarker)
            add(ContextPath(filePath.normalize().parent))
            add(FileName(filePath.normalize().fileName.toString()))
        })
    }

    fun pushFunctionScope() {
        push(Scope(top).apply { add(FunctionScopeMarker) })
    }

    fun pushReplScope() {
        push(Scope(top).apply { add(ReplScopeMarker) })
    }

    infix operator fun plusAssign(scope: AbstractScope) {
        push(scope)
    }

    override fun toString() = scopes.joinToString(separator = "\n")
}