package jaguar.core.compiler.scope

import jaguar.util.TypeMap
import java.nio.file.Paths
import kotlin.reflect.KClass

abstract class AbstractScope {
    abstract val parent: AbstractScope?
    open val isGlobalScope = false

    private val scopeData = TypeMap<ScopeAware>()

    operator fun <T : ScopeAware> set(kClass: KClass<T>, extension: T) {
        scopeData.put(kClass, extension)
        extension.setScope(this)
    }

    inline fun <reified T : ScopeAware> set(extension: T) = set(T::class, extension)
    inline fun <reified T : ScopeAware> add(extension: T) = set(T::class, extension)


    operator fun <T : ScopeAware> get(key: KClass<T>): T? {
        tailrec fun _get(s: AbstractScope, key: KClass<T>): T? =
                if (s.has(key)) {
                    s.scopeData[key]
                } else {
                    val parent = s.parent
                    if (parent != null) {
                        _get(parent, key)
                    } else {
                        null
                    }
                }
        return _get(this, key)
    }

    inline fun <reified T : ScopeAware> get() = get(T::class)

    fun <T : ScopeAware> has(key: KClass<T>) = scopeData.contains(key)
    inline fun <reified T : ScopeAware> has() = has(T::class)

    fun <T : ScopeAware> remove(key: KClass<T>): T? = scopeData.remove(key)
    inline fun <reified T : ScopeAware> remove(): T? = remove(T::class)

    /**
     * Gets closest object of the given type registered in this scope's parent, grandparent, etc.
     */
    fun <T : ScopeAware> getClosestAncestor(key: KClass<T>): T? {
        return parent?.get(key)
    }

    inline fun <reified T : ScopeAware> getClosestAncestor(): T? = getClosestAncestor(T::class)

    val imports: Imports get() = get<Imports>()!!

    override fun toString(): String {
        return "Scope@${this.hashCode()}{ ${scopeData.values.joinToString(separator = ", ")} }"
    }

    init {
        add(Imports())
        add(Dictionary())
    }
}

object GlobalScope : AbstractScope() {
    override val parent = null
    override val isGlobalScope = true

    init {
        add(imports {
            +"java.lang.Object"
            +"java.lang.*"
        })

        add(Dictionary().apply { addBuiltinWords() })
        add(ContextPath(Paths.get(".").toAbsolutePath()))
    }
}

class Scope(override val parent: AbstractScope?) : AbstractScope()