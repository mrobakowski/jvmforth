package jaguar.core.compiler

import jaguar.core.JLineReader
import jaguar.core.WordSplitter
import jaguar.core.compiler.scope.*
import jaguar.core.word.CompiledWord
import jaguar.util.escapeJavaLiteral
import uy.klutter.core.jdk7.exists
import java.nio.file.Paths
import kotlin.properties.Delegates

const val JAGUAR_LANG_NAME: String = "jaguar"

class Compiler {
    fun compileAndStartNext(): CompiledWord {
        val compiled = try {
            currentFunctionBuilder.compile()
        } finally {
            startMethod(force = true)
        }
        return compiled
    }

    fun startMethod(name: String? = null, force: Boolean = false) {
        if (!force) throwIfUnterminatedFun()
        currentFunctionBuilder = FunctionBuilder(this).apply { if (name != null) this.name = name }
    }

    fun throwIfUnterminatedFun() {
        if (!currentFunctionBuilder.isBuilt && !currentFunctionBuilder.body.isNullOrBlank())
            throw IllegalStateException("unterminated word encountered")
    }

    val dictionary: Dictionary
        get() = topScope.get<Dictionary>()!!

    var currentFunctionBuilder: FunctionBuilder = FunctionBuilder(this)
        private set

    var currentRawWord: String by Delegates.notNull<String>()
    var currentPackage: String = ""
        set(value) {
            if (!currentFunctionBuilder.isReplInvocation)
                currentFunctionBuilder.packageName = value
            field = value
        }

    val imports: Collection<String> get() = topScope.imports.values
    fun import_(importName: String) {
        var scope = topScope
        if (scope.isReplScope()) {
            scope = scope.parent!!
            assert(scope.isFileScope() || scope is GlobalScope)
        }
        scope.imports.import(importName)
    }

    fun lookupClass(className: String): Class<*>? = try {
        Class.forName(className)
    } catch (e: ClassNotFoundException) {
        try {
            Class.forName(topScope.imports[className])
        } catch (e: Exception) {
            throw Exception("Error finding class for name $className", e)
        }
    }

    val scopeStack = ScopeStack()
    val topScope: AbstractScope get() = scopeStack.top

    val promptWithName = "$JAGUAR_LANG_NAME> "
    val emptyPrompt = "${JAGUAR_LANG_NAME.map { ' ' }.joinToString("")}> "

    var prompt = promptWithName

    val input = if (runningFromIntelliJ()) WordSplitter() else WordSplitter(JLineReader(this))

    fun continuePrompt() {
        prompt = emptyPrompt
    }

    fun mainPrompt() {
        prompt = promptWithName
    }

    fun init(args: Array<String>) {
        val defaultInclude = if (System.getProperty("jaguar.home") != null) {
            val base = Paths.get(".").toAbsolutePath().normalize()
            val prelude = Paths.get(System.getProperty("jaguar.home"), "/std/prelude.jg").toAbsolutePath().normalize()
            base.relativize(prelude).toString().let {println(it); it}
        } else {
            "src/dist/std/prelude.jg"
        }

        val code = (listOf(defaultInclude) + args).map {
            """ include "${it.escapeJavaLiteral()}" """
        }.joinToString(separator = "\n", postfix = "\n")

        input.prepend(code)
    }

    companion object {
        @JvmStatic var currentCompiler by Delegates.notNull<Compiler>()
    }
}

private fun runningFromIntelliJ(): Boolean {
    val classPath = System.getProperty("java.class.path")
    return classPath.contains("IntelliJ IDEA")
}
