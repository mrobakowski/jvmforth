package jaguar.core

import jaguar.core.compiler.Compiler
import jaguar.util.StartupTimer
import jaguar.util.printStackTrace
import uy.klutter.core.common.initializedWith

fun main(args: Array<String>) {
    StartupTimer.start()
    Compiler().initializedWith {
        Compiler.currentCompiler = this
        init(args)
    }.apply {
        val c = this
        while (true) {
            try {
                val raw = c.input.readNextWord()
                currentRawWord = raw
                val word = raw.trim()
                dictionary.getWord(word)?.apply { executeInCompileTime(c) } ?:
                        throw RuntimeException("Word `$word` not found!")
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }
}

