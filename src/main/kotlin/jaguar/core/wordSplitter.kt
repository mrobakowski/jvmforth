package jaguar.core

import jaguar.core.compiler.Compiler
import jline.console.ConsoleReader
import java.io.InputStreamReader
import java.io.PushbackReader
import java.io.Reader
import java.util.*
import java.util.regex.Pattern

class WordSplitter(stdin: Reader = InputStreamReader(System.`in`)) : AutoCloseable {
    private val input = PushbackReader(stdin, 1000000) // one meg

    fun prepend(anotherInput: String) {
        input.unread(anotherInput.toCharArray())
    }

    fun readNextWord(): String {
        val strBuilder = StringBuilder()
        var ch: Char

        // appends all the leading whitespace and the first non-whitespace
        do {
            ch = input.read().toChar()
            strBuilder.append(ch)
        } while (ch.isWhitespace())

        // append all non-whitespace chars
        while (true) {
            ch = input.read().toChar()
            if (ch.isWhitespace()) {
                input.unread(ch.toInt())
                break
            }
            strBuilder.append(ch)
        }

        return strBuilder.toString()
    }

    fun popOneChar(): Char = input.read().toChar()

    fun readUntilMatches(pattern: Pattern): String {
        val strBuilder = StringBuilder()
        var ch: Char

        while (!pattern.matcher(strBuilder).matches()) {
            ch = input.read().toChar()
            strBuilder.append(ch)
        }

        return strBuilder.toString()
    }

    fun readUntil(pred: (CharSequence) -> Boolean): String {
        val strBuilder = StringBuilder()
        var ch: Char

        while (!pred(strBuilder)) {
            ch = input.read().toChar()
            strBuilder.append(ch)
        }

        return strBuilder.toString()
    }

    fun discardLeadingWhitespaces() {
        var ch: Char
        do {
            ch = input.read().toChar()
        } while (ch.isWhitespace())

        // last one read isn't whitespace
        input.unread(ch.toInt())
    }

    fun readWhileMatches(pattern: Pattern): String {
        val strBuilder = StringBuilder()
        var ch: Char

        do {
            ch = input.read().toChar()
            strBuilder.append(ch)
        } while (pattern.matcher(strBuilder).matches())

        input.unread(strBuilder.last().toInt())
        strBuilder.deleteCharAt(strBuilder.lastIndex)

        return strBuilder.toString()
    }

    fun readWhile(pred: (CharSequence) -> Boolean): String {
        val strBuilder = StringBuilder()
        var ch: Char

        do {
            ch = input.read().toChar()
            strBuilder.append(ch)
        } while (pred(strBuilder))

        input.unread(strBuilder.last().toInt())
        strBuilder.deleteCharAt(strBuilder.lastIndex)

        return strBuilder.toString()
    }

    override fun close() = input.close()
}

class JLineReader(val c: Compiler) : Reader() {
    private val buf = ArrayDeque<Char>()

    private val cr = ConsoleReader()
    init {
        cr.expandEvents = false
    }

    override fun close() {
        cr.shutdown()
    }

    override fun read(cbuf: CharArray, off: Int, len: Int): Int {
        if (buf.isEmpty()) addMore()
        var i = 0
        while (!buf.isEmpty() && i < len) {
            cbuf[off + i] = buf.removeFirst()
            i++
        }
        return i
    }

    private fun addMore() {
        val line = cr.readLine(c.prompt) + "\n"
        c.continuePrompt()
        buf.addAll(line.asSequence())
    }
}