package jaguar.util

import org.apache.commons.lang3.StringEscapeUtils

fun String.unescapeJavaLiteral() = StringUnescape.unescape_perl_string(this)!!
fun String.escapeJavaLiteral() = StringEscapeUtils.escapeJava(this)!!
fun trim(s: String) = s.trim()

fun String.escapeJaguarName() = capitalize().map {
    when (it) {
        '.' -> "${'$'}Dot"
        '-' -> "${'$'}Minus"
        '!' -> "${'$'}Bang"
        '@' -> "${'$'}At"
        '#' -> "${'$'}Hash"
        '$' -> "${'$'}Dollar"
        '%' -> "${'$'}Percent"
        '^' -> "${'$'}Caret"
        '&' -> "${'$'}And"
        '*' -> "${'$'}Star"
        '(' -> "${'$'}OpenParen"
        ')' -> "${'$'}CloseParen"
        '+' -> "${'$'}Plus"
        '=' -> "${'$'}Equals"
        '?' -> "${'$'}QuestionMark"
        '/' -> "${'$'}Slash"
        '\\' -> "${'$'}BackSlash"
        ';' -> "${'$'}Semicolon"
        '[' -> "${'$'}OpenBracket"
        ']' -> "${'$'}CloseBracket"
    // TODO: add more
        else -> it.toString()
    }
}.joinToString(separator = "")

fun Exception.printStackTrace() = ExceptionFormatter.printException(this)