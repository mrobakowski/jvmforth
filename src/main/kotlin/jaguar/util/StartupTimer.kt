package jaguar.util

import java.util.*

object StartupTimer {
    private var _start = 0L

    @JvmStatic fun start() {
        _start = System.currentTimeMillis()
    }

    @JvmStatic fun stop() {
        val res = (System.currentTimeMillis() - _start) / 1000.0
        println("Startup time: ${res.format(2)}s")
    }
}

private fun Double.format(digits: Int) = java.lang.String.format(Locale.ENGLISH, "%.${digits}f", this)!!