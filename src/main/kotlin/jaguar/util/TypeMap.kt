package jaguar.util

import kotlin.reflect.KClass

class TypeMap<SUPERTYPE : Any>(
        private val _s: MutableMap<KClass<out SUPERTYPE>, SUPERTYPE> = mutableMapOf()
) {
    inline fun <reified T : SUPERTYPE> get(): T? = get(T::class)
    inline fun <reified T : SUPERTYPE> put(payload: T): T? = put(T::class, payload)
    inline fun <reified T : SUPERTYPE> set(payload: T) = set(T::class, payload)
    inline fun <reified T : SUPERTYPE> remove(): T? = remove(T::class)
    inline fun <reified T : SUPERTYPE> contains() = contains(T::class)
    inline fun <reified T : SUPERTYPE> getOrPut(noinline defaultValue: () -> T): T = getOrPut(T::class, defaultValue)

    @Suppress("UNCHECKED_CAST")
    operator fun <T : SUPERTYPE> get(kClass: KClass<T>) = _s[kClass] as T?

    @Suppress("UNCHECKED_CAST")
    fun <T : SUPERTYPE> put(key: KClass<T>, value: T) = _s.put(key, value) as T?

    operator fun <T : SUPERTYPE> set(kClass: KClass<T>, value: T) = _s.set(kClass, value)

    @Suppress("UNCHECKED_CAST")
    fun <T : SUPERTYPE> remove(key: KClass<T>) = _s.remove(key) as T?

    fun <T : SUPERTYPE> contains(key: KClass<T>) = _s.contains(key)

    @Suppress("UNCHECKED_CAST")
    fun <T : SUPERTYPE> getOrPut(key: KClass<T>, defaultValue: () -> T) = _s.getOrPut(key, defaultValue) as T

    fun clear() = _s.clear()

    val keys: MutableSet<KClass<out SUPERTYPE>>
        get() = _s.keys

    val values: MutableCollection<SUPERTYPE>
        get() = _s.values
}