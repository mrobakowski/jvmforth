package jaguar

import jaguar.core.compiler.StackEffect
import jaguar.core.compiler.TypeError
import org.junit.Test

class TypeSystemTest {
    val SE = { a: List<Class<*>>, b: List<Class<*>> -> StackEffect(a, b) }

    fun <T> l(vararg elems: T) = listOf(*elems)

    @Test fun testTypeSystemSoundness() {
        assert(
                SE(l(), l(String::class.java)) + SE(l(Any::class.java), l(Integer::class.java))
                        == SE(l(), l(Integer::class.java))
        )

        assert(
                SE(l(Integer::class.java), l()) + SE(l(String::class.java), l())
                        == SE(l(String::class.java, Integer::class.java), l())
        )

        assert(
                SE(l(), l(String::class.java)) + SE(l(), l(Integer::class.java))
                        == SE(l(), l(String::class.java, Integer::class.java))
        )
    }

    @Test(expected = TypeError::class) fun testWrongTypes() {
        SE(l(String::class.java, Integer::class.java), listOf(String::class.java)) + SE(l(Integer::class.java), l())
    }

    @Test(expected = TypeError::class) fun testIncompatibleTypes() {
        SE(l(), l(Any::class.java)) + SE(l(String::class.java), l())
    }

    @Test fun testSince() {
        val first = StackEffect(listOf(Any::class.java, String::class.java), listOf(Int::class.javaObjectType))
        val firstPlusNext = first + StackEffect.consuming(Int::class.javaObjectType)
        val sum = firstPlusNext + StackEffect.producing(Boolean::class.java) + StackEffect.producing(Any::class.java)
        val diff = sum.since(first)
        val diff1 = sum.since(firstPlusNext)

        assert(diff == StackEffect(listOf(Int::class.javaObjectType), listOf(Boolean::class.javaObjectType, Any::class.java)))

        assert(diff1 == StackEffect(listOf(), listOf(Boolean::class.javaObjectType, Any::class.java)))
    }
}