package jaguar.std

import org.junit.Test

class TestExtractEvalFragment {
    @Test fun testEvalFragment() {
        assert(extractEvalFragments("dupa ${"$"}{") == listOf(Fragment.Literal("dupa ${"$"}{")))
        assert(extractEvalFragments("dupa ${"$"}{ eks{{}} di {}{}{}}${"$"}{lol} ${"$"}{ elo } xd ${"$"}{end} ") ==
                listOf(
                        Fragment.Literal("dupa "),
                        Fragment.Eval(" eks{{}} di {}{}{}"),
                        Fragment.Eval("lol"),
                        Fragment.Literal(" "),
                        Fragment.Eval(" elo "),
                        Fragment.Literal(" xd "),
                        Fragment.Eval("end"),
                        Fragment.Literal(" ")
                )
        )
    }
}