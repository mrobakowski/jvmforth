package jaguar.utilTest

import jaguar.util.TypeMap
import org.junit.Test

class TypeMapTest {
    @Test
    fun test() {
        val tm = TypeMap<Any>()

        data class Foo(val i: Int)
        data class Bar(val s: String)

        tm.put(Foo(1))
        tm.put(Bar("baz"))

        val foo: Foo = tm.get()!!
        val bar: Bar = tm.get()!!

        assert(foo == Foo(1))
        assert(bar == Bar("baz"))
    }
}
