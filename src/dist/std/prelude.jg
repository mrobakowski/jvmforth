macro: import
    compiler #getInput #readNextWord
    jaguar.util.StringUtilsKt#trim/1
    compiler #import_/1
;

include "macro.jg"

import java.nio.file.*
import java.util.*
import jaguar.core.Stack

// duplicate the top of the stack
macro: dup
    newIdentifier -> x
    `-> ${x}
    ${x} ${x}`
;

// discards the top of the stack
: pop -> x ;

// swaps the two top elements of the stack
macro: swap
    newIdentifier newIdentifier -> x, y
    `-> ${x}, ${y}
    ${y} ${x}`
;

// prints out the top of the stack and pops it
: .
    System.out #println(Object)
;

// exits the program
: bye
    0 System#exit
;

macro: emptyStringArray
    import jaguar.core.compiler.Compiler
    import jaguar.core.Stack
    import jaguar.core.compiler.StackEffect
    import jaguar.core.compiler.FunctionBuilder

    compiler
    java {
        jaguar.core.compiler.Compiler c = (jaguar.core.compiler.Compiler) Stack.pop();
        FunctionBuilder f = c.getCurrentFunctionBuilder();
        f.setBody(
            f.getBody() + " jaguar.core.Stack.push(new String[0]); "
        );
        Class[] classes = new Class[1];
        classes[0] = (new String[0]).getClass();
        f.setStackEffect(
            f.getStackEffect().plus(StackEffect.Companion.producing(classes))
        );
    }
;

: pwd
    "" emptyStringArray Paths#get/2 #toAbsolutePath #toString .
;

: strlen String#length ;

: = Objects#equals ;

: not if false else true then ;

: != = not ;

// clears the whole program stack
: clst Stack#clear ;

: assert if else AssertionError#new/0 throw then ;

: readUntil -> delim: String
    import jaguar.core.*
    import jaguar.core.compiler.*
    import jaguar.util.*
    Compiler#getCurrentCompiler #getInput -> in
    "" -> res

    begin
        in #readNextWord -> newWord
        newWord StringUtilsKt#trim/1 delim !=
    while
        res newWord s+ => res
    repeat

    res
;

macro: [
    "]" readUntil -> initString
    "," initString #split/1 Arrays#asList -> initExprs
    newIdentifier -> listIdent

    "" -> body
    initExprs #size -> count
    0 -> i
    begin i count < while
        i initExprs #get as String -> expr
        expr " " s+
            listIdent s+
            " #add/1 pop\n" s+
            body swap s+ => body
        1 i + => i
    repeat

    `
        java.util.ArrayList#new/0 -> ${listIdent}
        ${body}
        ${listIdent}
    `
;

[ 1, "foo" "bar" s+ ] ;

pop ;

: startupTestsEnabled true ;

macro: maybeIncludeTests
    startupTestsEnabled if
        `include "tests.jg"`
    then
;

maybeIncludeTests

"Prelude loaded!" . ;

jaguar.util.StartupTimer#stop ;